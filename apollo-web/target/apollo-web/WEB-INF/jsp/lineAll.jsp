<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/4/15
  Time: 22:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>柱形图展示</title>
    <script src="${pageContext.request.contextPath}/js/esl.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="screen" type="text/css"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600&subset=latin,latin-ext' rel='stylesheet'>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style2.css">
</head>
<body>
<div>
    <div class="container">
        <div class="row">
            <%--<span id="btn0" class="btn btn-small submit">返回</span>--%>
            <span id="btn1" class="btn btn-small submit">收支统计</span>
            <span id="btn2" class="btn btn-small submit">税务统计</span>
        </div>
    </div>
    <div class="years">
        <label id="lblSelect" class="lblSelect">
            <select id="selectPointOfInterest" class="selectPointOfInterest title="年份选择">
            <option  selected = "selected" value="1">收入</option>
            <option value="0">支出</option>
            </select>
        </label>
    </div>

    <div id="main" style="height:530px"></div>

</div>

</body>
<script>

    require.config({
        paths: {
            'echarts': '${pageContext.request.contextPath}/js/echarts',
            'echarts/chart/bar': '${pageContext.request.contextPath}/js/echarts',
            'echarts/chart/line': '${pageContext.request.contextPath}/js/echarts'
        }
    });

    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],
        function (ec) {

            var myChart = ec.init(document.getElementById('main'));

            option = {
                title : {
                    text: '年金额收支统计表',
                    subtext: '数据来自数据库(自拟)'
                },
                tooltip : {
                    trigger: 'axis'
                },
                legend: {
                    data: ['2014', '2015','2016']
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        boundaryGap : false,
                        data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        axisLabel : {
                            formatter: '{value}'
                        }
//                        boundaryGap: [0, 0.01]
                    }
                ],
                series : [
                    {
                        name:'2016',
                        type:'line',
                        data:[11, 11, 15, 13, 12, 13, 10],
                        markPoint : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                        markLine : {
                            data : [
                                {type : 'average', name: '平均值'}
                            ]
                        }
                    },
                    {
                        name:'2015',
                        type:'line',
                        data:[1, -2, 2, 5, 3, 2, 0],
                        markPoint : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                        markLine : {
                            data : [
                                {type : 'average', name : '平均值'}
                            ]
                        }
                    },
                    {
                        name:'2014',
                        type:'line',
                        data:[1, -2, 2, 5, 3, 2, 0],
                        markPoint : {
                            data : [
                                {type : 'max', name: '最大值'},
                                {type : 'min', name: '最小值'}
                            ]
                        },
                        markLine : {
                            data : [
                                {type : 'average', name : '平均值'}
                            ]
                        }
                    }
                ]
            };

            //默认加载

            $.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/ticket/ajaxApolloColLine.do",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({"year": 2014, "flag":"amount" ,"endyear" : 2016,"rule":2}),
                success: function (res) {
                    res[0].intcome.pop();
                    res[1].intcome.pop();
                    res[2].intcome.pop();
                    option.series[2].data = res[0].intcome.reverse();
                    option.series[1].data = res[1].intcome.reverse();
                    option.series[0].data = res[2].intcome.reverse();
//                    option.series[1].data = res.outcome.reverse();
                    myChart.setOption(option);
                }
            });


            //点击按钮1 amount
            $("#btn1").click(function () {
                $.ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/ticket/ajaxApolloColLine.do",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({"year": 2014, "flag":"amount" ,"endyear" : 2016,"rule":2}),
                    success: function (res) {
                        res[0].intcome.pop();
                        res[1].intcome.pop();
                        res[2].intcome.pop();
                        option.series[2].data = res[0].intcome.reverse();
                        option.series[1].data = res[1].intcome.reverse();
                        option.series[0].data = res[2].intcome.reverse();
//                    option.series[1].data = res.outcome.reverse();
                        myChart.setOption(option);
                    }
                });

            });

            //点击按钮2

            $("#btn2").click(function () {

                $.ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/ticket/ajaxApolloColLine.do",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({"year": 2014, "flag":"taxmoney" ,"endyear" : 2016,"rule":2}),
                    success: function (res) {
                        option.title.text =  '年税务收支统计表';
                        res[0].intcome.pop();
                        res[1].intcome.pop();
                        res[2].intcome.pop();
                        option.series[2].data = res[0].intcome.reverse();
                        option.series[1].data = res[1].intcome.reverse();
                        option.series[0].data = res[2].intcome.reverse();
                        myChart.setOption(option);
                    }
                });
            });

            //选择盈亏
            $('#selectPointOfInterest').change(function(){
                var inout=$("#selectPointOfInterest").children('option:selected').val();//
                var flag = option.title.text == '年金额收支统计表' ? "amount": "taxmoney";
                $.ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/ticket/ajaxApolloColLine.do",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({"year": 2014, "flag":"taxmoney" ,"endyear" : 2016,"rule":2}),
                    dataType: "json",
                    success: function (res) {
                        if(inout ==1){
                            res[0].intcome.pop();
                            res[1].intcome.pop();
                            res[2].intcome.pop();
                            option.series[2].data = res[0].intcome.reverse();
                            option.series[1].data = res[1].intcome.reverse();
                            option.series[0].data = res[2].intcome.reverse();
                            myChart.setOption(option);
                        }
                        else{
                            res[0].outcome.pop();
                            res[1].outcome.pop();
                            res[2].outcome.pop();
                            option.series[2].data = res[0].outcome.reverse();
                            option.series[1].data = res[1].outcome.reverse();
                            option.series[0].data = res[2].outcome.reverse();
                            myChart.setOption(option);
                        }
                    }
                });
            });

            $("#btn0").click(function () {
                window.location.href="${pageContext.request.contextPath}/index.jsp"
            });
        }
    );

</script>
</html>