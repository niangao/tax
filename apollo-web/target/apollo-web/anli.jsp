<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!--easyui的主题文件-->
<link rel="stylesheet" type="text/css"
	href="easyui/themes/default/easyui.css">
<!--Easyui组件的图片样式文件-->
<link rel="stylesheet" type="text/css" href="easyui/themes/icon.css">
<!--JQuery主文件-->
<script type="text/javascript" src="easyui/jquery.min.js"></script>
<!--easyui主文件-->
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<style>
#aa div a {
	width: 80%;
	height: auto;
	background: pink;
	display: block;
	margin-top: 5px;
}
</style>
<script>
	$(function() {
		$("a[name=nm]").click(function() {
			//alert("TTTTT");

			//获得当前链接的文本内容
			var title = $(this).html();

			//获得当前组件的href属性
			var url = $(this).attr("href");

			addTab(title, url);
			return false;
		})
	})
	function addTab(title, url) {
		if ($("#tt").tabs("exists", title)) {
			$('#tabs').tabs('select', title);
		} else {
			$("#tt").tabs("add", {
				title : title,
				content : createFrame(url)
			});
		}
	}
	function createFrame(url) {
		var s = '<iframe src="'
				+ url
				+ '" height="100%" width="100%" scrolling="auto"  style="height:100%;width:100%" frameborder="0"></iframe>';
		return s;
	}
</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',title:'',split:false"
		style="height:30px;">
		<span style="float:right;margin-right:30px">欢迎**** |<a
			class="easyui-linkbutton" data-options="iconCls:'icon-no'" href="#">退出</a>
		</span>
	</div>
	<div data-options="region:'south',title:'',split:true"
		style="height:50px;">
		<div style="text-align:center;line-height:45px">******@qq.com</div>
	</div>

	<div data-options="region:'west',title:'导航管理',split:false"
		style="width:190px;">
		<div id="aa" class="easyui-accordion"
			style="width:300px;height:200px;" data-options="fit:true">
			<div title="柱形图展示" data-options=""
				style="overflow:auto;padding:10px;">
				<a name="nm" href="${pageContext.request.contextPath}/ticket/toColumn.do">一年柱形图展示</a>
				<a name="nm" href="${pageContext.request.contextPath}/ticket/toColumnAll.do">总年柱形图展示</a>
			</div>
			<div title="饼状图展示" data-options="" style="padding:10px;">
				<a name="nm" href="${pageContext.request.contextPath}/ticket/toPie.do" >一年饼状图展示</a>
				<a name="nm" href="${pageContext.request.contextPath}/ticket/toPieAll.do" >总年饼状图展示</a>
			</div>
			<div title="折线图展示">
				<a name="nm" href="${pageContext.request.contextPath}/ticket/toLine.do" >一年饼状图展示</a>
				<a name="nm" href="${pageContext.request.contextPath}/ticket/toLineAll.do" >总年饼状图展示</a>
			</div>
		</div>

	</div>
	<div data-options="region:'center',title:'center title'"
		style="padding:5px;background:#eee;">
		<div id="tt" class="easyui-tabs" style="width:500px;height:250px;"
			data-options="fit:true">
			<div title="首页">首页的内容</div>
		</div>

	</div>
</body>


</html>