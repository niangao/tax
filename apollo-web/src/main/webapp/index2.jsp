<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/4/13
  Time: 22:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>欢迎使用税务分析系统</title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style3.css" media="screen" type="text/css" />
    <script src="js/jquery-1.9.1.min.js"></script>
</head>
<body>
<%--<div style="text-align:center;clear:both">--%>
<%--</div>--%>
<div class="myheader">
    <h1>欢迎使用税务分析系统</h1>
    <h2>制作团队:年糕队</h2>
</div>
<div class="radmenu"><a href="#" class="show" >START</a>
    <ul>
        <li>
            <a href="${pageContext.request.contextPath}/ticket/toTable.do" class="skip">表格展示</a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/ticket/toColumn.do" class="skip">一年柱形图展示</a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/ticket/toColumnAll.do" class="skip">总年柱形图展示</a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/ticket/toPie.do" class="skip">一年饼状图展示</a>
        </li>
        <li>
            <a href="${pageContext.request.contextPath}/ticket/toPieAll.do" class="skip">总年饼状图展示</a>
        </li>
    </ul>
</div>

</body>
</html>
<script src="js/index.js"></script>
<script type="text/javascript">
    $(".skip").click(function(){
        if($(this).attr("id")=="zhong"){
            $(this).bind("click");
            $(this).attr("id","");
        }else{
            $(this).attr("id","zhong");
            window.location.href=$(this).attr("href");
        }
      window.location.href=this.attr("href");
    });
</script>

