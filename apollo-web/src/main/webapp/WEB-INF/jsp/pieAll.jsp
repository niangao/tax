<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>总年饼状图展示</title>
    <script src="${pageContext.request.contextPath}/js/esl.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="screen" type="text/css"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600&subset=latin,latin-ext' rel='stylesheet'>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style2.css">
</head>
<body>
<div>
    <div class="container">
        <div class="row">
            <%--<span id="btn0" class="btn btn-small submit">返回</span>--%>
            <span id="btn1" class="btn btn-small submit">收支统计</span>
            <span id="btn2" class="btn btn-small submit">税务统计</span>
        </div>
    </div>

    <div style="content:'.';
                 height:20px;
                 visibility:hidden;
                 display:block;
                 clear:both;  ">
    </div>
    <div id="main" style="height:500px; width:500px ;margin-left: 5%; display: inline-block"; ></div>
    <div id="main2" style="height:500px; width:500px ; margin-left: 5%;  display: inline-block"></div>

</div>

</body>
<script>

    require.config({
        paths: {
            'echarts': '${pageContext.request.contextPath}/js/echarts',
            'echarts/chart/bar': '${pageContext.request.contextPath}/js/echarts',
            'echarts/chart/line': '${pageContext.request.contextPath}/js/echarts'
        }
    });

    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],
        drawEcharts
    );
    function drawEcharts(ec){
        drawPie(ec);
    }
    function drawPie (ec) {
        var myChart = ec.init(document.getElementById('main'));
        var myChart2 = ec.init(document.getElementById('main2'));
        option = {
            title: {
                text: '年金额收入饼状图',
                subtext: '数据来自数据库(自拟)',
                x: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ['2014年', '2015年', '2016年']
            },
            series: [
                {
                    name: '详细信息',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '60%'],
                    data: [
                        {value: 0, name: '2014年'},
                        {value: 0, name: '2015年'},
                        {value: 0, name: '2016年'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        option2 = {
            title: {
                text: '年金额支出饼状图',
                subtext: '数据来自数据库(自拟)',
                x: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ['2014年', '2015年', '2016年']
            },
            series: [
                {
                    name: '访问来源',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '60%'],
                    data: [
                        {value: 0, name: '2014年'},
                        {value: 0, name: '2015年'},
                        {value: 0, name: '2016年'},
                    ],
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        $.ajax({
            type: "POST",
            url: "${pageContext.request.contextPath}/ticket/ajaxApolloColAll.do",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({"year": 2014, "flag":"amount" ,"endyear" : 2016}),
            success: function (res) {

                option.series[0].data[0].value = res.intcome[2];
                option.series[0].data[1].value = res.intcome[1];
                option.series[0].data[2].value = res.intcome[0];

                option2.series[0].data[0].value = res.outcome[2];
                option2.series[0].data[1].value = res.outcome[1];
                option2.series[0].data[2].value = res.outcome[0];
                myChart.setOption(option,true);
                myChart2.setOption(option2,true);
            }
        });
        $("#btn1").click(function () {
            $.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/ticket/ajaxApolloColAll.do",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({"year": 2014, "flag":"amount" ,"endyear" : 2016}),
                dataType: "json",
                success: function (res) {
                    option.title.text = "年金额收入饼状图";
                    option2.title.text = "年金额支出饼状图";
                    option.series[0].data[0].value = res.intcome[2];
                    option.series[0].data[1].value = res.intcome[1];
                    option.series[0].data[2].value = res.intcome[0];
                    option2.series[0].data[0].value = res.outcome[2];
                    option2.series[0].data[1].value = res.outcome[1];
                    option2.series[0].data[2].value = res.outcome[0];
                    myChart.setOption(option,true);
                    myChart2.setOption(option2,true);
                }
            });
        });
        $("#btn2").click(function () {
            $.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/ticket/ajaxApolloColAll.do",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({"year": 2014, "flag":"taxmoney" ,"endyear" : 2016}),
                dataType: "json",
                success: function (res) {
                    option.title.text =  '年税务收入饼状图';
                    option2.title.text =  '年税务支出饼状图';
                    option.series[0].data[0].value = res.intcome[2];
                    option.series[0].data[1].value = res.intcome[1];
                    option.series[0].data[2].value = res.intcome[0];
                    option2.series[0].data[0].value = res.outcome[2];
                    option2.series[0].data[1].value = res.outcome[1];
                    option2.series[0].data[2].value = res.outcome[0];
                    myChart.setOption(option,true);
                    myChart2.setOption(option2,true);
                }
            });
        });
    }
    $("#btn0").click(function () {
        window.location.href="${pageContext.request.contextPath}/index.jsp"
    });
</script>
</html>