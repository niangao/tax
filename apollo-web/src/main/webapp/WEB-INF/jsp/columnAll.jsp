<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/4/15
  Time: 22:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>总年柱形图展示</title>
    <script src="${pageContext.request.contextPath}/js/esl.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" media="screen" type="text/css"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600&subset=latin,latin-ext' rel='stylesheet'>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/style2.css">
</head>
<body>
<div>
    <div class="container">
        <div class="row">
            <%--<span id="btn0" class="btn btn-small submit">返回</span>--%>
            <span id="btn1" class="btn btn-small submit">收支统计</span>
            <span id="btn2" class="btn btn-small submit">税务统计</span>
        </div>
        <div class="years">
            <label id="lblSelect" class="lblSelect">
                <select id="selectPointOfInterest" class="selectPointOfInterest" title="年份选择">
                    <option  selected = "selected" value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                </select>
            </label>
            <label id="lblSelect2"  class="lblSelect">
                <select id="selectPointOfInterest2" class="selectPointOfInterest" title="年份选择">
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option  selected = "selected" value="2016">2016</option>
                </select>
            </label>
        </div>
        <span id="btn3" class="btn btn-small submit">查询</span>
    </div>

    <div id="main" style="height:530px"></div>

</div>

</body>
<script>

    require.config({
        paths: {
            'echarts': '${pageContext.request.contextPath}/js/echarts',
            'echarts/chart/bar': '${pageContext.request.contextPath}/js/echarts',
            'echarts/chart/line': '${pageContext.request.contextPath}/js/echarts'
        }
    });

    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],
        function (ec) {

            var myChart = ec.init(document.getElementById('main'));

            option = {
                title: {
                    text: '总金额收支统计表',
                    subtext: '数据来自数据库(自拟)'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                legend: {
                    data: ['总收入', '总支出']
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: {
                    type: 'value',
                    boundaryGap: [0, 0.01]
                },
                yAxis: {
                    type: 'category',
                    data: ['2016年','2015年','2014年']
                },
                series: [
                    {
                        name: '总收入',
                        type: 'bar',
                        data: [] //左边是16年
                    },
                    {
                        name: '总支出',
                        type: 'bar',
                        data: []
                    }
                ]
            };

            myChart.setOption(option);
            //默认加载
            $.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/ticket/ajaxApolloColAll.do",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({"year": 2014, "flag":"amount" ,"endyear" : 2016}),
                success: function (res) {
                    option.series[0].data = res.intcome;
                    option.series[1].data = res.outcome;
                    myChart.setOption(option);
                }
            });

            //点击按钮3 查询
            $("#btn3").click(function () {
                var flag = option.title.text == '总金额收支统计表' ? "amount": "taxmoney";//flag的值
                var year=$("#selectPointOfInterest").children('option:selected').val();//这就是selected的值
                var endyear=$("#selectPointOfInterest2").children('option:selected').val();//这就是selected的值
                if(endyear < year){
                    var temp = endyear;
                    endyear = year;
                    year = temp;
                }
                $.ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/ticket/ajaxApolloColAll.do",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({"year": year, "flag":flag ,"endyear" : endyear}),
                    dataType: "json",
                    success: function (res) {
                        var yearArray = []
                        for(var i=endyear; i >= year ; i--){
                            yearArray.push(i);
                        }
                        option.yAxis.data=yearArray;
                        option.series[0].data = res.intcome;
                        option.series[1].data = res.outcome;
                        myChart.setOption(option);
                    }
                });
            });

            //点击按钮1 查询
            $("#btn1").click(function () {
                var year=$("#selectPointOfInterest").children('option:selected').val();//这就是selected的值
                var endyear=$("#selectPointOfInterest2").children('option:selected').val();//这就是selected的值
                $.ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/ticket/ajaxApolloColAll.do",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({"year": year, "flag":"amount" ,"endyear" : endyear}),
                    dataType: "json",
                    success: function (res) {
                        option.title.text =  '总金额收支统计表';
                        option.series[0].data = res.intcome;
                        option.series[1].data = res.outcome;
                        option.series[0].name = '总收入';
                        option.series[1].name = '总支出';
                        option.legend.data = ['总收入', '总支出'];
                        myChart.setOption(option);
                    }
                });
            });

            $("#btn2").click(function () {
                var year=$("#selectPointOfInterest").children('option:selected').val();//这就是selected的值
                var endyear=$("#selectPointOfInterest2").children('option:selected').val();//这就是selected的值
                $.ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/ticket/ajaxApolloColAll.do",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({"year": year, "flag":"taxmoney" ,"endyear" : endyear}),
                    dataType: "json",
                    success: function (res) {
                        option.title.text =  '总税务收支统计表';
                        option.series[0].data = res.intcome;
                        option.series[1].data = res.outcome;
                        option.series[0].name = '总税收(收入)';
                        option.series[1].name = '总税收(支出)';
                        option.legend.data = ['总税收(收入)', '总税收(支出)'];
                        myChart.setOption(option);
                    }
                });
            });

            $("#btn0").click(function () {
                history.back(-1);
            });
        }
    );

</script>
</html>