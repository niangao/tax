package com.kk.apollo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Administrator on 2017/5/8.
 */
@Controller
public class BaseController {
    @RequestMapping(value="/admin/{toJsp}")
    public String admin(@PathVariable("toJsp") String someJsp){
        return "admin/"+someJsp;
    }
    @RequestMapping(value="/base/{toJsp}")
    public String base(@PathVariable("toJsp") String someJsp){
        return "base/"+someJsp;
    }
    @RequestMapping(value="/common/{toJsp}")
    public String common(@PathVariable("toJsp") String someJsp){
        return "common/"+someJsp;
    }
    @RequestMapping(value="/portal/{toJsp}")
    public String portal(@PathVariable("toJsp") String someJsp){
        return "portal/"+someJsp;
    }
    @RequestMapping(value="/qupai/{toJsp}")
    public String qupai(@PathVariable("toJsp") String someJsp){
        return "qupai/"+someJsp;
    }


}
