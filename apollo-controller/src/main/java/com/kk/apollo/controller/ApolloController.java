package com.kk.apollo.controller;

import com.kk.apollo.biz.model.Apollo;
import com.kk.apollo.biz.model.ApolloAllVO;
import com.kk.apollo.biz.model.ApolloColAjax;
import com.kk.apollo.biz.model.ApolloColumn;
import com.kk.apollo.biz.service.ApolloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Administrator on 2017/4/15.
 */
@Controller
@RequestMapping("/ticket")
public class ApolloController {
    @Autowired
    ApolloService apolloService;

    @RequestMapping("toHome")
    public String toHome(Model model) {
        return "common/home";
    }
    @RequestMapping("toHome2")
    public String toHome2(Model model) {
        return "common/index";
    }

    @RequestMapping("findById")
    public String findById(int id, Model model) {
        Apollo apollo = apolloService.findById(id);
        model.addAttribute("apollo", apollo);
        return "test";
    }

    @RequestMapping("findAll")
    public String findAll(Model model) {
        List<Apollo> apollos = apolloService.apolloList();
        model.addAttribute("apollo", apollos);
        return "test";
    }

    //跳转到pie
    @RequestMapping("toPie")
    public String toPie(Model model) {
        List<Apollo> apollos = apolloService.apolloList();
        return "pie";
    }

    //跳转到table
    @RequestMapping("toTable")
    public String toTable(Model model) {
        List<Apollo> apollos = apolloService.apolloList();
        model.addAttribute("apollos", apollos);
        return "table";
    }
    //跳转到line
    @RequestMapping("toLine")
    public String toLine(Model model) {
        return "line";
    }
    @RequestMapping("toLineAll")
    public String toLineAll(Model model) {
        return "lineAll";
    }

    //跳转到column
    @RequestMapping("toColumn")
    public String toColumn(Model model) {
        return "column";
    }

    // 跳转到columnAll
    @RequestMapping("toColumnAll")
    public String toColumnAll(Model model) {
        return "columnAll";
    }

    // 跳转到pieAll
    @RequestMapping("toPieAll")
    public String toPieAll(Model model) {
        return "pieAll";
    }

    //查询指定年份期间的收入支出
    @RequestMapping("ajaxApolloColAll")
    public @ResponseBody
    ApolloColAjax ajaxApolloColAll(@RequestBody Map<String, String> map) {
        Integer year = Integer.parseInt(map.get("year"));
        String flag = map.get("flag");
        if (year == null || year == 0) {
            year = 2015;
        }
        Integer endyear = Integer.parseInt(map.get("endyear"));
        if (endyear == null || endyear == 0) {
            endyear = 2016;
        }
        if(endyear < year){
            int temp = endyear;
            endyear = year;
            year = temp;
        }
        //创建Ajax对象
        ApolloColAjax apolloColAjax = new ApolloColAjax();
        List<Double> list1 = new ArrayList<>();
        List<Double> list2 = new ArrayList<>();

        for (int i = endyear; i >= year; i--) {
            List<ApolloAllVO> ApolloAllVOs = null;
            //ApolloVallVOs 第一个对象Qmoney是支出 第二个是支出(收入)
            if("amount".equals(flag)){
                ApolloAllVOs = this.apolloService.findSumByYear(i);
            }else if ("taxmoney".equals(flag)) {
                ApolloAllVOs = this.apolloService.findTaxByYear(i);
            }

            if(ApolloAllVOs!=null&&ApolloAllVOs.size()!=0){
                list1.add(ApolloAllVOs.get(1).getQmoney());//16年
                list2.add(ApolloAllVOs.get(0).getQmoney());//15年
            }
        }
//        String flag = map.get("flag");
        //设置收入支出
        apolloColAjax.setIntcome(list1);
        apolloColAjax.setOutcome(list2);
        return apolloColAjax;
    }


    //进行ajax的数据传递
    @RequestMapping("ajaxApolloCol")
    public @ResponseBody
    ApolloColAjax ajaxApolloCol(@RequestBody Map<String, String> map) {
        Integer year =null;
        Integer rule =null;
        String flag = map.get("flag");
        if(!map.containsKey("year")&&map.get("year")==null&&map.get("year").equals("")){
            year = 2016;
        }
        year= Integer.parseInt(map.get("year"));
        if(!map.containsKey("rule")&&map.get("rule")==null && map.get("rule").equals("")){
            rule=2;
        }
        rule = Integer.parseInt(map.get("rule"));
        List<ApolloColumn> apolloColumnList = getApolloColumnList(12);
        ApolloColAjax apolloColAjax = getApolloColAjax(getApolloColumnList(apolloColumnList, flag, year),rule);
        return apolloColAjax;
    }

    //进行ajax的数据传递
    @RequestMapping("ajaxApolloColLine")
    public @ResponseBody
    List<ApolloColAjax> ajaxApolloColLine(@RequestBody Map<String, String> map) {
        List<ApolloColAjax> apolloColAjaxList = new ArrayList<>();
        Integer year = Integer.parseInt(map.get("year"));
        String flag = map.get("flag");
        if (year == null || year == 0) {
            year = 2015;
        }
        Integer endyear = Integer.parseInt(map.get("endyear"));
        if (endyear == null || endyear == 0) {
            endyear = 2016;
        }
        if(endyear < year){
            int temp = endyear;
            endyear = year;
            year = temp;
        }
        Integer rule =null;
        rule = Integer.parseInt(map.get("rule"));
        List<ApolloColumn> apolloColumnList = getApolloColumnList(12);
        for(int i = year ; i <=endyear ; i ++){
            ApolloColAjax apolloColAjax = getApolloColAjax(getApolloColumnList(apolloColumnList, flag, i),rule);
            apolloColAjaxList.add(apolloColAjax);
        }
        System.out.print(apolloColAjaxList.toString());
        return apolloColAjaxList;
    }

    //apolloColumnList中初始化月份
    public List<ApolloColumn> getApolloColumnList(int size) {
        List<ApolloColumn> apolloColumnList = new ArrayList<>(12);
        for (int i = 0; i < size; i++) {
            ApolloColumn apolloColumn = new ApolloColumn();
            apolloColumn.setMonth(i);
            apolloColumn.setIncome((double) 0);
            apolloColumn.setOutcome((double) 0);
            apolloColumnList.add(apolloColumn);
        }
        return apolloColumnList;
    }

    //返回用于传递ajax的ApolloColAjax对象  里面有两个list(rule = 2 是全查 1是收入>支出 0是支出>收入)
    public ApolloColAjax getApolloColAjax(List<ApolloColumn> apolloColumnList,int rule) {
        ApolloColAjax apolloColAjax = new ApolloColAjax();
        List<Double> list1 = new ArrayList<>();//存放收入
        List<Double> list2 = new ArrayList<>();//存放支出
        List<String> list3 = new ArrayList<>();//存放月份
        double sumin = 0;
        double sumout = 0;
        //先放12月
        for (int i = apolloColumnList.size(); i > 0; i--) {
            Double income = apolloColumnList.get(i - 1).getIncome();
            Double outcome = apolloColumnList.get(i - 1).getOutcome();
            if(rule==1){
                if(income<=outcome){
                    continue;
                }
            }else if(rule==0){
                if(income>= outcome ){
                    continue;
                }
            }
            list1.add(income);
            sumin = sumin + income;
            list2.add(outcome);
            sumout = sumout+ outcome;
            list3.add((i+"月"));
        }
        list3.add("总收入支出");
        list2.add(sumout);
        list1.add(sumin);
        apolloColAjax.setIntcome(list1);
        apolloColAjax.setOutcome(list2);
        apolloColAjax.setMonths(list3);
        return apolloColAjax;
    }

    //将数据按月份分离 分离后属性: 月份 月收入 月支出
    public List<ApolloColumn> getApolloColumnList(List<ApolloColumn> apolloColumnList, String flag, int year){
        List<Apollo> apollos = apolloService.findByYear(year);
        for (Apollo apollo : apollos) {
            ApolloColumn temp = apolloColumnList.get(apollo.getMydate().getMonth());
            if (temp.getMonth() == null) {
                temp.setMonth(temp.getMonth());
            }

            if (apollo.getMytype() == 1) {
                if ("amount".equals(flag)) {
                    //收入
                    temp.setIncome(temp.getIncome() + apollo.getAmount());
                } else if ("taxmoney".equals(flag)) {
                    temp.setIncome(temp.getIncome() + apollo.getTaxmoney());
                }
            } else {
                //支出
                if (flag.equals("amount")) {
                    temp.setOutcome(temp.getOutcome() + apollo.getAmount());
                } else if (flag.equals("taxmoney")) {
                    temp.setOutcome(temp.getOutcome() + apollo.getTaxmoney());
                }
            }
        }
        return  apolloColumnList;
    }

}
