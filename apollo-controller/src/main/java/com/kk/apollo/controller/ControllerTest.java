package com.kk.apollo.controller;

import com.kk.apollo.biz.model.ApolloColAjax;
import com.kk.apollo.biz.model.ApolloColumn;
import com.kk.apollo.biz.model.TestModel;
import com.kk.apollo.biz.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.kk.apollo.biz.*;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by Kirito on 2017/4/11.
 */

@Controller
@RequestMapping("/test")
public class ControllerTest {
    @Autowired
    TestService testService;

    @RequestMapping("test")
    public String hello(){
        return "test";
    }

    public @ResponseBody
    String ajaxApolloCol(@RequestBody Map<String, String> map) {
//        try {
//
//            String realpath = ServletActionContext.getServletContext().getRealPath("/file");
//
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//
//            String imgName = sdf.format(new Date()) + ".png";
//
//            String filePath = realpath+Constants.SF_FILE_SEPARATOR+imgName;
//
//            OutputStream out = new FileOutputStream(filePath);
//
//            QRCode.GenerateImage(img,out);//生成图片
//
//            message   = QRCode.decoderQRCode(filePath);
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//
//        }

        return "null";
    }

}
