package com.kk.apollo.biz.test.service;

import com.kk.apollo.biz.model.TestModel;
import com.kk.apollo.biz.service.TestService;
import com.kk.apollo.biz.test.AbstractTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Kirito on 2017/4/11.
 */
public class TestServiceTest extends AbstractTest {
    @Autowired
    TestService testService;

    @Test
    public void testService() {
        String orgId = testService.getOrgIdById(new TestModel(), 1);
        System.err.println(orgId);
    }
}
