package com.kk.apollo.biz.model;

import lombok.Data;
import lombok.ToString;

/**
 * Created by Kirito on 2017/4/9.
 */
@Data
@ToString
public class TestModel {

    private Integer id;

    private String orgId;


}
