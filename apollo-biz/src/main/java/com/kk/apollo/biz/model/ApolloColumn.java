package com.kk.apollo.biz.model;

import lombok.Data;
import lombok.ToString;

/**
 * Created by Administrator on 2017/4/16.
 */

@Data
@ToString
public class ApolloColumn {
    private Integer month;

    private Double income;

    private Double outcome;

}
