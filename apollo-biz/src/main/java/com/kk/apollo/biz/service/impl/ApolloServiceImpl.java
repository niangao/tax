package com.kk.apollo.biz.service.impl;

import com.kk.apollo.biz.dao.ApolloDao;
import com.kk.apollo.biz.model.Apollo;
import com.kk.apollo.biz.model.ApolloAllVO;
import com.kk.apollo.biz.model.ApolloColumn;
import com.kk.apollo.biz.model.ApolloExample;
import com.kk.apollo.biz.service.ApolloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/4/15.
 */
@Service
public class ApolloServiceImpl implements ApolloService {
    @Autowired
    ApolloDao apolloDao;

    @Override
    public List<Apollo> apolloList() {
        ApolloExample apolloExample = new ApolloExample();
        return apolloDao.findAll();
    }

    public Apollo findById(int id) {
        return apolloDao.selectByPrimaryKey(id);
    }

    @Override
    public List<Apollo> findByYear(int year) {
        return apolloDao.findByYear(year);

    }

    @Override
    public List<ApolloAllVO> findSumByYear(int year) {
        return apolloDao.findSumByYear(year);
    }

    @Override
    public List<ApolloAllVO> findTaxByYear(int year) {
        return this.apolloDao.findTaxByYear(year);
    }

}
