package com.kk.apollo.biz.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * Created by Administrator on 2017/4/16.
 */
@Data
@ToString
public class ApolloColAjax {
    private List<Double> intcome;
    private List<Double> outcome;
    private List<String> months;

}
