package com.kk.apollo.biz.service.impl;

import com.kk.apollo.biz.dao.TestDao;
import com.kk.apollo.biz.model.TestModel;
import com.kk.apollo.biz.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Kirito on 2017/4/10.
 */

@Component("testServiceImpl")
public class TestServiceImpl implements TestService {
    @Autowired
    TestDao testDao;

    @Override
    public String getOrgIdById(TestModel testModel, int id) {
        testModel = testDao.selectById(id);
        return testModel.getOrgId();
    }
}
