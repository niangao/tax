package com.kk.apollo.biz.model;

import lombok.Data;
import lombok.ToString;

/**
 * Created by Administrator on 2017/4/17.
 */
@Data
@ToString
public class ApolloAllVO {
    private Double qmoney;
    private int qyear;
}
