package com.kk.apollo.biz.service;

import com.kk.apollo.biz.model.TestModel;

/**
 * Created by Kirito on 2017/4/9.
 */
public interface TestService {
    String getOrgIdById (TestModel testModel, int id);
}
