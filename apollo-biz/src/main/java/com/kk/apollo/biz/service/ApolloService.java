package com.kk.apollo.biz.service;

import com.kk.apollo.biz.model.Apollo;
import com.kk.apollo.biz.model.ApolloAllVO;
import com.kk.apollo.biz.model.ApolloColumn;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/4/15.
 */
public interface ApolloService {
    public List<Apollo> apolloList();

    public Apollo findById(int id);

    List<Apollo> findByYear(int year);

    public List<ApolloAllVO> findSumByYear(int year);

    List<ApolloAllVO> findTaxByYear(int i);
}
