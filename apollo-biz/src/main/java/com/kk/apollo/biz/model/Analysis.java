package com.kk.apollo.biz.model;

import lombok.Data;
import lombok.ToString;

/**
 *查询统计对象
 */
@Data
@ToString
public class Analysis {
    //年度统计

    double maxAmount;//最大金额
    double minAmount;//最小金额
    double maxTax;//最大税收
    double minTax;//最小税收
    double avgAmount;//平均金额
    double avgTax;//平均税收
    double AmountAll;//总金额
    double TaxAll;//总税收


    /*
        定义一个方法
        金额最小值及其对应的年份
        税收最大值及其对应的年份
        税收最小值及其对应的年份
        平均收入
        平均税收
        盈利年份
        亏损年份
     */
    //月度统计

}
