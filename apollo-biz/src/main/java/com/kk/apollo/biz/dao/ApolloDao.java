package com.kk.apollo.biz.dao;

import com.kk.apollo.biz.model.Apollo;
import com.kk.apollo.biz.model.ApolloAllVO;
import com.kk.apollo.biz.model.ApolloExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ApolloDao {
    int countByExample(ApolloExample example);

    int deleteByExample(ApolloExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Apollo record);

    int insertSelective(Apollo record);

    List<Apollo> selectByExample(ApolloExample example);

    Apollo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Apollo record, @Param("example") ApolloExample example);

    int updateByExample(@Param("record") Apollo record, @Param("example") ApolloExample example);

    int updateByPrimaryKeySelective(Apollo record);

    int updateByPrimaryKey(Apollo record);

    List<Apollo> findAll();

    List<Apollo> findByYear(int year);

    List<ApolloAllVO> findSumByYear(int year);

    List<ApolloAllVO> findTaxByYear(int year);

//    List<Apollo> findMkMoney(int year);
//
//    List<Apollo> findLsMoney(int year);
}