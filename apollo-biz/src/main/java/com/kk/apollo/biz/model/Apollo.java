package com.kk.apollo.biz.model;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class Apollo {
    private Integer id;

    private Date mydate;

    private Double amount;

    private Double taxmoney;

    private Integer mytype;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getMydate() {
        return mydate;
    }

    public void setMydate(Date mydate) {
        this.mydate = mydate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getTaxmoney() {
        return taxmoney;
    }

    public void setTaxmoney(Double taxmoney) {
        this.taxmoney = taxmoney;
    }

    public Integer getMytype() {
        return mytype;
    }

    public void setMytype(Integer mytype) {
        this.mytype = mytype;
    }
}