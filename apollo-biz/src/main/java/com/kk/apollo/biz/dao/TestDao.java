package com.kk.apollo.biz.dao;

import com.kk.apollo.biz.model.TestModel;
import org.springframework.stereotype.Repository;

/**
 * Created by Kirito on 2017/4/9.
 */
public interface TestDao {
    TestModel selectById(int id);
}
